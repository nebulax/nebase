
#ifndef NEB_EVDP_IO_SOCKET_H
#define NEB_EVDP_IO_SOCKET_H 1

#include <nebase/cdefs.h>

/**
 * \brief log sockerr and then return close
 */
extern neb_evdp_cb_ret_t neb_evdp_sock_log_on_hup(int fd, void *udata, const void *context);

/**
 * \brief get the sockerr for the socket fd
 */
extern int neb_evdp_sock_get_sockerr(const void *context, int *sockerr)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));

#endif
