
cmake_policy(PUSH)
cmake_policy(SET CMP0056 NEW)
cmake_policy(SET CMP0057 NEW)

set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_STANDARD 11)

#
# Set C Compile Flags
#

set(NeBase_C_FLAGS "-O3")
set(NeBase_C_HARDEN_FLAGS "")

unset(HAVE_C_MACRO_MALLOC_EXTENDED CACHE)

if(COMPAT_CODE_COVERAGE)
  if(CMAKE_BUILD_TYPE NOT EQUAL "Debug")
    message(SEND_ERROR "Code coverage is only supported in Debug mode")
  endif()
endif(COMPAT_CODE_COVERAGE)

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS "4.9")
    message(SEND_ERROR "GCC version >= 4.9 is required")
  endif()

  if(CMAKE_C_COMPILER_VERSION VERSION_GREATER_EQUAL "11.0")
    set(HAVE_C_MACRO_MALLOC_EXTENDED ON CACHE INTERNAL "C malloc(dealloctor, ptr-index) support")
  endif()

  if(OSTYPE_SUN)
    # force to use m64 for SunOS
    set(NeBase_C_FLAGS "${NeBase_C_FLAGS} -m64")
  endif()
  set(NeBase_C_FLAGS "${NeBase_C_FLAGS} -Wall -Wextra -Wformat -Wformat-security -Werror=format-security")

  if(WITH_HARDEN_FLAGS)
    set(NeBase_C_HARDEN_FLAGS "${NeBase_C_HARDEN_FLAGS} -fstack-protector-strong")
  endif()

  if(COMPAT_CODE_COVERAGE)
    set(NeBase_C_FLAGS "${NeBase_C_FLAGS} -fprofile-arcs -ftest-coverage -O0")
    # analyze with gcov/lcov/gcovr
  endif()

elseif(CMAKE_C_COMPILER_ID STREQUAL "Clang")

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS "3.7")
    message(SEND_ERROR "Clang version >= 3.7 is required")
  endif()

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS "13.0")
    message(AUTHOR_WARNING "we need to check malloc attribute support for this compiler")
  else()
    set(HAVE_C_MACRO_MALLOC_EXTENDED ON CACHE INTERNAL "C malloc(dealloctor, ptr-index) support")
  endif()

  set(NeBase_C_FLAGS "${NeBase_C_FLAGS} -Wall -Wextra -Wformat -Wformat-security -Werror=format-security")

  if(WITH_HARDEN_FLAGS)
    set(NeBase_C_HARDEN_FLAGS "${NeBase_C_HARDEN_FLAGS} -fstack-protector-strong")
  endif()

  if(COMPAT_CODE_COVERAGE)
    set(NeBase_C_FLAGS "${NeBase_C_FLAGS} -fprofile-instr-generate -fcoverage-mapping -O0")
    # analyze with llvm-cov
  endif()

elseif(CMAKE_C_COMPILER_ID STREQUAL "SunPro")
  message(SEND_ERROR "SunPro CC (Oracle Developer Studio) is not supported")
elseif(CMAKE_C_COMPILER_ID STREQUAL "IntelLLVM")
  # TODO test with cmake >= 3.20

elseif(CMAKE_C_COMPILER_ID STREQUAL "Intel")
  message(SEND_ERROR "The classic icc compiler is not supported, use icx instead")
elseif(CMAKE_C_COMPILER_ID STREQUAL "NVHPC")
  # TODO test with cmake >= 3.20 on CPU with AVX support

elseif(CMAKE_C_COMPILER_ID STREQUAL "PGI")
  message(SEND_ERROR "The classic pgcc compiler is not supported, use nvc instead")
elseif(CMAKE_C_COMPILER_ID STREQUAL "IBMClang")
  # TODO test with cmake >= 3.23

elseif(CMAKE_C_COMPILER_ID STREQUAL "XL")
  # TODO test clang based xlc and original xlc with cmake >= 3.15

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS "16.1")
    message(SEND_ERROR "XL C version >= 16.1 is required")
  endif()

  message(AUTHOR_WARNING "we need to check malloc attribute support for this compiler")

  set(IBMXL_WARNOFF_FLAGS "-Wno-attributes")
  set(NeBase_C_FLAGS "${NeBase_C_FLAGS} ${IBMXL_WARNOFF_FLAGS}")

  if(WITH_HARDEN_FLAGS)
    set(NeBase_C_HARDEN_FLAGS "${NeBase_C_HARDEN_FLAGS} -qstackprotect=strong")
  endif()

  if(COMPAT_CODE_COVERAGE)
    message(SEND_ERROR "No code coverage support with IBM XL C Compiler")
    # no command line support for at least 16.1.1
  endif()

elseif(CMAKE_C_COMPILER_ID STREQUAL "AppleClang")

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS "7.0.0")
    # for the coresponding llvm version, see src/CMakeLists.txt in
    #    https://opensource.apple.com/source/clang/
    message(SEND_ERROR "Clang version >= 7.0.0 (based on llvm 3.7) is required")
  endif()

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS "13.0.0")
    message(AUTHOR_WARNING "we need to check malloc attribute support for this compiler")
  else()
    set(HAVE_C_MACRO_MALLOC_EXTENDED ON CACHE INTERNAL "C malloc(dealloctor, ptr-index) support")
  endif()

  set(NeBase_C_FLAGS "${NeBase_C_FLAGS} -Wall -Wextra -Wformat -Wformat-security -Werror=format-security")

  if(WITH_HARDEN_FLAGS)
    set(NeBase_C_HARDEN_FLAGS "${NeBase_C_HARDEN_FLAGS} -fstack-protector-strong")
  endif()

  if(COMPAT_CODE_COVERAGE)
    set(NeBase_C_FLAGS "${NeBase_C_FLAGS} -fprofile-instr-generate -fcoverage-mapping -O0")
  endif()

else()
  message(SEND_ERROR "Unsupported C Compiler")
endif()

set(CMAKE_C_FLAGS "${NeBase_C_FLAGS} ${CMAKE_C_FLAGS}")
if(WITH_HARDEN_FLAGS)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${NeBase_C_HARDEN_FLAGS}")
endif(WITH_HARDEN_FLAGS)

# Generate Position-independent code
if(WITH_HARDEN_FLAGS)
  # set -pie while linking exe
  cmake_policy(SET CMP0083 NEW)
  include(CheckPIESupported)
  check_pie_supported(OUTPUT_VARIABLE _pie_output LANGUAGES C)
  if(NOT CMAKE_C_LINK_PIE_SUPPORTED)
    message(WARNING "PIE is not supported at link time: ${_pie_output}.\n"
                    "PIE link options will not be passed to linker.")
  endif()

  # this will set -fPIC/-fPIE according to the target type
  set(CMAKE_POSITION_INDEPENDENT_CODE TRUE)
endif(WITH_HARDEN_FLAGS)

#
# Detect pthread, and export NebulaX::Threads
#

set(OLD_CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
set(CMAKE_C_FLAGS "${NeBase_C_FLAGS} ${NeBase_C_HARDEN_FLAGS}")
set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)
find_package(Threads REQUIRED)
if(NOT TARGET NebulaX::Threads)
  add_library(NebulaX::Threads INTERFACE IMPORTED GLOBAL)
  target_link_libraries(NebulaX::Threads INTERFACE Threads::Threads)
endif()
set(CMAKE_C_FLAGS "${OLD_CMAKE_C_FLAGS}")

#
# Set ld link flags
#

macro(_NebLoadCCompiler_test_ld_flag _flag)
  set(SAFE_CMAKE_REQUIRED_QUIET ${CMAKE_REQUIRED_QUIET})
  set(SAFE_CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}")
  set(CMAKE_REQUIRED_QUIET ON)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${_flag}")
  include(CheckCCompilerFlag)

  # clear both the normal variable and the cache variable
  unset(NeBase_LD_FLAG_OK)
  unset(NeBase_LD_FLAG_OK CACHE)

  check_c_compiler_flag("${_flag}" NeBase_LD_FLAG_OK)
  set(CMAKE_REQUIRED_QUIET ${SAFE_CMAKE_REQUIRED_QUIET})
  set(CMAKE_EXE_LINKER_FLAGS "${SAFE_CMAKE_EXE_LINKER_FLAGS}")
endmacro(_NebLoadCCompiler_test_ld_flag)

macro(_NebLoadCCompiler_get_ld_option _opt)
  set(NeBase_LD_REAL_FLAG "${_opt}")
  _NebLoadCCompiler_test_ld_flag(${NeBase_LD_REAL_FLAG})
  if(NOT NeBase_LD_FLAG_OK)
    set(NeBase_LD_REAL_FLAG "-Wl,${_opt}")
    _NebLoadCCompiler_test_ld_flag(${NeBase_LD_REAL_FLAG})
    if(NOT NeBase_LD_FLAG_OK)
      set(NeBase_LD_REAL_FLAG "")
    endif()
  endif()
  if(NeBase_LD_REAL_FLAG)
    message(STATUS "Linker flag for ${_opt}: ${NeBase_LD_REAL_FLAG}")
  else()
    message(STATUS "Linker flag for ${_opt}: unsupported")
  endif()
endmacro(_NebLoadCCompiler_get_ld_option)

macro(_NebLoadCCompiler_get_ld_z_option)
  set(NeBase_LD_REAL_FLAG "-z ${_opt}")
  _NebLoadCCompiler_test_ld_flag(${NeBase_LD_REAL_FLAG})
  if(NOT NeBase_LD_FLAG_OK)
    set(NeBase_LD_REAL_FLAG "-Wl,z,${_opt}")
    _NebLoadCCompiler_test_ld_flag(${NeBase_LD_REAL_FLAG})
    if(NOT NeBase_LD_FLAG_OK)
      set(NeBase_LD_REAL_FLAG "")
    endif()
  endif()
  if(NeBase_LD_REAL_FLAG)
    message(STATUS "Linker flag for -z ${_opt}: ${NeBase_LD_REAL_FLAG}")
  else()
    message(STATUS "Linker flag for -z ${_opt}: unsupported")
  endif()
endmacro(_NebLoadCCompiler_get_ld_z_option)

macro(_NebLoadCCompiler_set_linker_flag)
  include(NeBaseDetectLinker)

  set(NeBase_LD_FLAGS "")
  set(NeBase_LD_HARDEN_FLAGS "")

  set(NeBase_LD_OPTIONS "--xxx-assert-failed;--as-needed;-rdynamic")

  set(GNU_COMPATIBLE_LINKERS "GNU.bfd;GNU.gold;LLVM.lld")
  if(NeBase_LINKER_ID IN_LIST GNU_COMPATIBLE_LINKERS)
    set(NeBase_LD_Z_HARDEN_KEYWORDS "relro;now")
  elseif(NeBase_LINKER_ID STREQUAL "SUN.ld")
    set(NeBase_LD_Z_HARDEN_KEYWORDS "nodeferred")
  endif()

  foreach(_opt IN LISTS NeBase_LD_OPTIONS)
    _NebLoadCCompiler_get_ld_option(${_opt})
    set(NeBase_LD_FLAGS "${NeBase_LD_FLAGS} ${NeBase_LD_REAL_FLAG}")
  endforeach()
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${NeBase_LD_FLAGS}")
  set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} ${NeBase_LD_FLAGS}")
  set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${NeBase_LD_FLAGS}")

  if(WITH_HARDEN_FLAGS)
    foreach(_opt IN LISTS NeBase_LD_Z_HARDEN_KEYWORDS)
      _NebLoadCCompiler_get_ld_z_option(${_opt})
      set(NeBase_LD_HARDEN_FLAGS "${NeBase_LD_HARDEN_FLAGS} ${NeBase_LD_REAL_FLAG}")
    endforeach()
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${NeBase_LD_HARDEN_FLAGS}")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} ${NeBase_LD_HARDEN_FLAGS}")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${NeBase_LD_HARDEN_FLAGS}")
  endif(WITH_HARDEN_FLAGS)
endmacro(_NebLoadCCompiler_set_linker_flag)

_NebLoadCCompiler_set_linker_flag()

#
# Setup static analyzers
#

if(WITH_CLANG_TIDY)
  set(CMAKE_C_CLANG_TIDY "${CLANG_TIDY_EXE}" "-header-filter=.*")
endif(WITH_CLANG_TIDY)

if(WITH_CPPCHECK)
  set(CMAKE_C_CPPCHECK "${CPPCHECK_EXE}" "--language=c" "--std=c11" "--enable=warning,performance")
endif(WITH_CPPCHECK)

cmake_policy(POP)
